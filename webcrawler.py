from bs4 import BeautifulSoup
import requests
from database import Database


class Crawler:

    def __init__(self, url):
        self.db = Database()
        self.backup_url = url

    def crawl_url(self, url):
        try:
            get_url = requests.get(url)
            status = get_url.status_code
            if status == 200:
                html_code = get_url.content

                self.db.insert_webpage(url, html_code, status)
                soup = BeautifulSoup(html_code, 'html.parser')

                for link in soup.find_all('a'):

                    webpage_link = link.get('href')
                    if webpage_link.find('http') == -1:
                        webpage_link = self.backup_url + webpage_link

                    self.db.add_link(url, webpage_link)

                    if self.db.does_webpage_exist(webpage_link) is True:
                        self.crawl_url(webpage_link)

            else:
                print("page not found")
        except:
            print("Error")


# if __name__ == "__main__":
#     url = "https://blogging.com"
#     web_crawl = Crawler(url)
#     web_crawl.crawl_url(url)

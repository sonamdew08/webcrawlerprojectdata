import requests
import mysql.connector
from mysql.connector import Error


class Database:

    def __init__(self):

        self.connection = mysql.connector.connect(host='localhost',
                                                  database='test_crawler_db',
                                                  user='root',
                                                  password='admin1234')

    # def close_connection(self, connection, cursor):
    #     if(self.connection.is_connected()):
    #         cursor.close()
    #         self.connection.close()

    def does_webpage_exist(self, url):
        query = "select url from web_page_url where url = '{}'".format(
            url)
        record = self.fetch_all_rows(query)
        if record == []:
            return True
        return False

    def insert_webpage(self, url, content, status):
        sql_insert_query = """ INSERT INTO `web_page_url`
                        (`url`, `html_code`, `status`) VALUES (%s,%s,%s)"""
        insert_tuple = (url, content, status)
        self.execute_query(sql_insert_query, insert_tuple)

    def add_link(self, from_url, to_url):
        sql_insert_query = """ INSERT INTO `web_page_from_to_url`
                        (`from_url`, `to_url`) VALUES (%s,%s)"""
        insert_tuple = (from_url, to_url)
        self.execute_query(sql_insert_query, insert_tuple)

    def execute_query(self, query, args):
        cursor = self.connection.cursor()
        cursor.execute(query, args)
        self.connection.commit()

    def get_webpage_links(self):
        sql_select_Query = "select * from web_page_from_to_url"
        records = self.fetch_all_rows(sql_select_Query)

        return records

    def fetch_all_rows(self, sql_query):
        cursor = self.connection.cursor()
        cursor.execute(sql_query)
        records = cursor.fetchall()
        return records

import requests
import mysql.connector
from mysql.connector import Error
from database import Database


class Test_crawler_database:

    def __init__(self):

        self.connection = mysql.connector.connect(host='localhost',
                                                  user='root',
                                                  password='admin1234')

    def create_database(self):
        cursor = self.connection.cursor()
        cursor.execute("CREATE DATABASE test_crawler_db")

    def create_table(self):
        cursor = self.connection.cursor()
        cursor.execute("USE test_crawler_db")
        cursor.execute(
            "CREATE TABLE web_page_url (url VARCHAR(255) PRIMARY KEY, html_code LONGTEXT, status INT)")
        cursor.execute(
            "CREATE TABLE web_page_from_to_url (from_url LONGTEXT, to_url LONGTEXT)")

    def delete_database(self):
        cursor = self.connection.cursor()
        cursor.execute("DROP DATABASE test_crawler_db")

    def get_url(self):
        get_db = Database()
        query = "select url from test_crawler_db.web_page_url"
        records = get_db.fetch_all_rows(query)

        return records

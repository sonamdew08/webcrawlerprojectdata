import unittest
from webcrawler import Crawler
import shortest_path as sp
from test_database import Test_crawler_database

class WebcrawlerTest(unittest.TestCase):

      
    def setUp(self):
        self.db = Test_crawler_database()
        self.db.create_database()
        self.db.create_table()
        url = 'http://localhost:8000/'
        web_crawl = Crawler(url)
        web_crawl.crawl_url(url)

    def test_crawler(self):
        url = self.db.get_url()
        url_list = [url[0] for url in url]
        expected_url = [
            'http://localhost:8000/', 
        'http://localhost:8000/fifth.html', 
        'http://localhost:8000/fourth.html', 
        'http://localhost:8000/second.html', 
        'http://localhost:8000/third.html'
        ]
        self.assertEqual(url_list, expected_url)

    def test_shortest_path(self):
        shortest_path = sp.shorter_path('http://localhost:8000/', 'http://localhost:8000/fourth.html')
        expected_path = ['http://localhost:8000/', 'http://localhost:8000/second.html', 'http://localhost:8000/fourth.html']
        self.assertEqual(shortest_path, expected_path)

    def tearDown(self):
        self.db.delete_database()
        pass


if __name__ == '__main__':    

    unittest.main()
    


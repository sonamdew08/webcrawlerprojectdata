import networkx as nx
from database import Database

def shorter_path(url_a, url_b):
    database_obj = Database()
    try:
        
        graph = nx.DiGraph()
        rows = database_obj.get_webpage_links()
        # print(rows)
        for row in rows:
            from_url, to_url = row
            graph.add_edge(from_url, to_url)
        
        path = nx.shortest_path(graph, url_a, url_b)
        
        return path
    except:
        print("Path Not Found")


# if __name__ == "__main__":
#     database_obj = Database()
 # print(shorter_path('https://blogging.com/how-to-start-a-blog/make-money/','https://blogging.com/wordpress-hosting/fastcomet/'))
